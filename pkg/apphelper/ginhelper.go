package apphelper

import (
	"gitee.com/zhenxinma/gocommon/pkg/xcommon"
	"gitee.com/zhenxinma/gocommon/pkg/xlogging"
	"github.com/gin-gonic/gin"
)

// GetXLog return xlog.
func GetXLog(c *gin.Context) *xlogging.Entry {
	xlogInterface, ok := c.Get("xlog")
	if !ok {
		xlog := xlogging.Tag(xcommon.GetUUID())
		c.Set("xlog", xlog)
		return xlog
	}
	return xlogInterface.(*xlogging.Entry)
}
