// Package test.
// creator 2022-03-23 00:24:36
// Author  zhenxinma.
package main

import (
	"fmt"

	"gitee.com/zhenxinma/go_common/pkg/xstring"
)

func main() {
	str := "hello word"
	data := xstring.StringToSliceByte(str)
	fmt.Println(data)
	s := xstring.SliceByteToString(data)
	fmt.Println(s)
}
