package common_static

// @author: mazhenxin.
// @create: 2021-09-14 08:42:08

// COS相关.
const (
	// DefaultConfigFileName 默认配置文件的路径.
	DefaultConfigFileName = "config"
	// DefaultConfigPath 用于进行扫描的配置文件的路径.
	DefaultConfigPath = "./etc/"
	// DefaultConfigFileType 默认配置文件的类型.
	DefaultConfigFileType = "yaml"

	CosUrlPre = "https://"
)

// 时间转换常用的模板.
const (
	Template1 = "2006-01-02 15:04:05"
	Template2 = "2006/01/02 15:04:05"
	Template3 = "2006-01-02"
	Template4 = "20060102"
	Template5 = "15:04:05"
)
