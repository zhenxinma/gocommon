package common_time

// @author: mazhenxin.
// @create: 2021-09-14 08:41:24
import (
	"time"

	"gitee.com/zhenxinma/gocommon/src/common_static"
)

// @author: mazhenxin.
// @create: 2021-09-12 20:42:48

func GetNowTime(t uint) string {

	switch t {
	case 1:
		return time.Now().Format(common_static.Template1)
	case 2:
		return time.Now().Format(common_static.Template2)
	case 3:
		return time.Now().Format(common_static.Template3)
	case 4:
		return time.Now().Format(common_static.Template4)
	case 5:
		return time.Now().Format(common_static.Template5)
	default:
		return time.Now().Format(common_static.Template1)
	}
}

// GetNowTimeYmdTs 获取当前时间表示年月日的时间戳.
func GetNowTimeYmdTs() string {
	return time.Now().Format(common_static.Template4)
}
