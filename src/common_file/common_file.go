// Package common_file 一些和File相关的通用文件.
// @author: zhenxinma.
// @create: 2021-10-09 23:34:32
package common_file

import "os"

// TODO: common 不需要对外提供对象来进行访问,这点和Java中的静态方法思想类似.

// FileExist 判断当前指定路径上的文件是否存在.
func FileExist(path string) bool {

	_, err := os.Lstat(path)
	return !os.IsNotExist(err)
}
